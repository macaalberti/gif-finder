import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Gif, SearchGifsResponse } from '../interface/gifs.interface';

@Injectable({
  providedIn: 'root'
})
export class GifsService {

  private apiKey: string = '9TB3JAtw8xN8maYdswUD9Ct7JnoOd9nC';

  private urlService: string = 'https://api.giphy.com/v1/gifs';

  private _searchHistory: string[] = [];

  public results: Gif[] = [];

  get searchHistory(){
    return [...this._searchHistory]; //rompe la referencia
  }

  constructor(private http: HttpClient){
    
    this._searchHistory = JSON.parse(localStorage.getItem('searchHistory')!) || [];

    this.results = JSON.parse(localStorage.getItem('results')!) || [];

    //if(localStorage.getItem('searchHistory')){
      //this._searchHistory = JSON.parse(localStorage.getItem('searchHistory')!) || [];  
    //}

  }

  searchGifs( query: string){
    
    query = query.trim().toLowerCase();

    if(!this._searchHistory.includes(query)){
      this._searchHistory.unshift(query);
      this._searchHistory = this._searchHistory.splice(0,10);

      localStorage.setItem('searchHistory' , JSON.stringify(this._searchHistory));
    }
    
    const params = new HttpParams()
                          .set('api_key' , this.apiKey)
                          .set('limit' , '20')
                          .set('q' , query);


    this.http.get<SearchGifsResponse >(`${this.urlService}/search` , {params: params})
    .subscribe((response) => {
      this.results = response.data;
      localStorage.setItem('results' , JSON.stringify(this.results));
    });

  }

}
