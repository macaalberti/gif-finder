import { Component, ElementRef, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { GifsService } from '../services/gifs-service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: [
  ]
})
export class SearchComponent {

  @ViewChild('searchTxt') 
  searchTxt!: ElementRef<HTMLInputElement>;

  constructor(private gifsService: GifsService){}

  search(){
    
    const value = this.searchTxt.nativeElement.value;

    if(value.trim().length === 0){
      return;
    }

    this.gifsService.searchGifs(value);
    
    this.searchTxt.nativeElement.value = '';

  }

}
