import { Component, OnInit } from '@angular/core';
import { GifsService } from '../../gifs/services/gifs-service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent{

  constructor(private gifsServices: GifsService){}

  get searchHistory(){
    return this.gifsServices.searchHistory;
  }

  buscar(term: string){
    this.gifsServices.searchGifs(term);
  }

}
